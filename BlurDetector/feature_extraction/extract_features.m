% localBlurScore -- Extract blur feature for a fixed scale
%
%   Paras: 
%   @im        : Input grayscale image.
%   @patchsize : Parameter controlling local patch size.
%
%   The Code is created based on the method described in the following paper 
%   [1] "Discriminative Blur Detection Features", Jianping Shi, Li Xu, Jiaya Jia,
%       IEEE Conference on Computer Vision and Pattern Recognition, 2014. 
%   The code and the algorithm are for non-comercial use only.
%  
%   Author: Jianping Shi (jpshi@cse.cuhk.edu.hk)
%   Date  : 03/27/2014
%   Edited: Alexander Abugaliev (a.abugaliev@innopolis.ru)
%   Date  : 07/27/2016
%   Version : 1.0 
%   Copyright 2014, The Chinese University of Hong Kong.
%   http://www.shijianping.me/blur_cvpr14.pdf


function extract_features(im_name, patchsize)
    if ~ismember(patchsize,[11,15,21])
        exception = MException('extract_features:wrong_patchsize','Patchsize should be equal 11,15 or 21.');
        throw(exception);
    end
    
    patchsize = double(patchsize);
    load(strcat(im_name,'.mat'));

    % Read linear filters
    load('linear_filter_map.mat');
    W = linear_filter_map(patchsize);

    % Size characteristics for future feature resizing
    im_height = size(im,1); 
    im_width =  size(im,2);
    offset = (patchsize - 1)/2;
    x_start = offset+1;  x_end = im_width-offset;
    y_start = offset+1;  y_end = im_height-offset;
    datasize = (x_end-x_start+1)*(y_end-y_start+1);

    % Feature Extraction
    LSD = LocalSaturationDeviation(im, patchsize);
    im = rgb2gray(im);
    LK = LocalKurtosis(im, patchsize);
    GHS = GradientHistogramSpan(im, patchsize);
    LPSS = LocalPowerSpectrumSlope(im, patchsize); 
    LLF = LocalLearnedFilter(im, W(:,1:5));       

    % Resize features to original image size
    data = zeros(datasize,6);
    data(:,1) = reshape(LK(y_start:y_end,x_start:x_end),datasize,1);
    data(:,2) = reshape(GHS(y_start:y_end,x_start:x_end),datasize,1);
    data(:,3) = reshape(LPSS(y_start:y_end,x_start:x_end),datasize,1);
    data(:,4) = reshape(LLF{1}(y_start:y_end,x_start:x_end),datasize,1);
    data(:,5) = reshape(LLF{2}(y_start:y_end,x_start:x_end),datasize,1);
    data(:,6) = reshape(LSD(y_start:y_end,x_start:x_end),datasize,1);

    % Replace nan values
    for i = 1:6
        idx = isnan(data(:,i));
        if (sum(idx(:)) == im_height* im_width)
            data(:,i) = 0;
        elseif (sum(idx)>0)
            data(idx,i) = min(data(~idx, i));
        end
    end

    % Combine features for sending
    LK = reshape(data(:,1),y_end-y_start+1,x_end-x_start+1);
    GHS = reshape(data(:,2),y_end-y_start+1,x_end-x_start+1);
    LPSS = reshape(data(:,3),y_end-y_start+1,x_end-x_start+1);
    LLF1 = reshape(data(:,4),y_end-y_start+1,x_end-x_start+1);
    LLF2 = reshape(data(:,5),y_end-y_start+1,x_end-x_start+1);
    LSD = reshape(data(:,6),y_end-y_start+1,x_end-x_start+1);
    save('features.mat','LK','GHS','LPSS','LLF1','LLF2','LSD');