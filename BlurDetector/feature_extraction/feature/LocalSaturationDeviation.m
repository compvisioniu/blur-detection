function result = LocalSaturationDeviation(im,patchsize)
    him = rgb2hsv(im);
    him = him(:,:,2);
    [im_height, im_width] = size(him);
    offset = (patchsize - 1)/2;
    him = im2col(him, [patchsize, patchsize]);
    LSD = zeros(1,size(him,2));
    for i = 1 : size(LSD,2)
        LSD(i) = std(std(reshape(him(:,i),[patchsize,patchsize])));
    end
    LSD = reshape(LSD, [im_height-patchsize+1, im_width-patchsize+1]);
    result = padarray(LSD, [offset, offset], 'replicate');
end

