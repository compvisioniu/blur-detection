import numpy as np
import pandas as pd
import xgboost as xgb
from sklearn.feature_extraction.image import extract_patches_2d
import os

path_size = 5

booster = xgb.Booster()
booster.load_model('./stacking_separated_solid.bin')
classifier = xgb.XGBClassifier()
classifier._Booster = booster
	
classifiers={'21': xgb.XGBClassifier(),
             '15': xgb.XGBClassifier(),
             '11': xgb.XGBClassifier()}
classifiers_solid={'21': xgb.XGBClassifier(),
             '15': xgb.XGBClassifier(),
             '11': xgb.XGBClassifier()}

booster21 = xgb.Booster()
booster15 = xgb.Booster()
booster11 = xgb.Booster()
booster21.load_model('./standart_classifiers/classifier21.bin')
booster15.load_model('./standart_classifiers/classifier15.bin')
booster11.load_model('./standart_classifiers/classifier11.bin')
booster21_solid = xgb.Booster()
booster15_solid = xgb.Booster()
booster11_solid = xgb.Booster()
booster21_solid.load_model('./blur_solid_classifiers/classifier21.bin')
booster15_solid.load_model('./blur_solid_classifiers/classifier15.bin')
booster11_solid.load_model('./blur_solid_classifiers/classifier11.bin')

classifiers['21']._Booster = booster21
classifiers['15']._Booster = booster15
classifiers['11']._Booster = booster11
classifiers_solid['21']._Booster = booster21_solid
classifiers_solid['15']._Booster = booster15_solid
classifiers_solid['11']._Booster = booster11_solid

feature_list = ['GHS','LK','LLF1','LLF2','LPSS','LST']
label_mark = 'GT'

def read_prepared_data():
    X = pd.DataFrame()
    y = pd.DataFrame()
    for i,dirname in enumerate(dirlist):
        X_one_image = pd.read_csv('./extracted/'+dirname+'/stacking_data_wosolid.csv')
        X_one_image = X_one_image.iloc[range(0,X_one_image.shape[0],2)]
        X_one_image_solid = pd.read_csv('./extracted/'+dirname+'/stacking_data_osolid.csv')
        X_one_image_solid = X_one_image_solid.iloc[range(0,X_one_image.shape[0],2)]
        X_one_image = pd.concat([X_one_image,X_one_image_solid],axis=1)
        y_one_image = pd.read_csv('./extracted/'+dirname+'/stacking_gt_wosolid.csv')
        y_one_image = y_one_image.iloc[range(0,y_one_image.shape[0],2)]
        X = pd.concat([X,X_one_image],ignore_index=True)  
        y = pd.concat([y,y_one_image],ignore_index=True)
        print(i,dirname,X.shape[0])
    return X,y

def read_im_data(dirname,size):
    offset = int((21-int(size))/2)
    size = str(size)
    one_image_data = pd.DataFrame(data=None,columns=feature_list)
    for feature_name in feature_list:
        feature_data = pd.read_csv('./extracted/'+dirname+'/'+size+'/'+feature_name+'.csv',header=-1)
        feature_size = (feature_data.values.shape[0]-offset*2)*(feature_data.values.shape[1]-offset*2)
        if offset!=0:
            one_image_data[feature_name] = list(map(lambda x: x[0],feature_data.values[offset:-offset,offset:-offset].reshape(feature_size,1)))
        else:
            one_image_data[feature_name] = list(map(lambda x: x[0],feature_data.values.reshape(feature_size,1)))
    one_image_labels = pd.read_csv('./extracted/'+dirname+'/'+size+'/'+label_mark+'.csv',header=-1)
    im_size = one_image_labels.shape
    im_size = (im_size[0] - offset*2,im_size[1] - offset*2)
    if offset!=0:
        one_image_labels = list(map(lambda x: x[0],one_image_labels.values[offset:-offset,offset:-offset].reshape(feature_size,1)))
    else:
        one_image_labels = list(map(lambda x: x[0],one_image_labels.values.reshape(feature_size,1)))
    one_image_labels = pd.DataFrame(data=one_image_labels,columns=[label_mark])
    return one_image_data,one_image_labels,im_size

def im2col(image,patch_size):
    patches = extract_patches_2d(image,patch_size)
    return list(map(lambda x: x.reshape(1,patch_size[0]*patch_size[1])[0],patches))

patch_size = 5
def read_test_data(dirname,size,columns,columns_solid):
    size = str(size)
    label_mark = 'y'
    one_image_data,one_image_labels,im_size = read_im_data(dirname,size)
    one_image_result = classifiers[size].predict_proba(one_image_data)
    one_image_result_solid = classifiers_solid[size].predict_proba(one_image_data)

    one_image_blur = one_image_result[:,1].reshape(im_size)
    one_image_clear = one_image_result[:,0].reshape(im_size)
    one_image_solid = one_image_result_solid[:,1].reshape(im_size)
    
    one_image_blur = im2col(one_image_blur,(patch_size,patch_size))
    one_image_clear = im2col(one_image_clear,(patch_size,patch_size))
    one_image_solid = im2col(one_image_solid,(patch_size,patch_size))

    one_image_data = np.hstack((one_image_blur,one_image_clear))
    X = pd.DataFrame(one_image_data,columns=columns)
    X_solid = pd.DataFrame(one_image_solid,columns=columns_solid)

    one_image_labels = one_image_labels.values
    one_image_labels = one_image_labels.reshape(im_size)[int(path_size/2):-int(path_size/2),int(path_size/2):-int(path_size/2)]
    one_image_labels = one_image_labels.reshape(one_image_labels.shape[0]*one_image_labels.shape[1],1)

    y = pd.DataFrame(one_image_labels,columns=[label_mark])
    return X,X_solid,y,im_size

def read_full_test_data(dirname):
    X_full = pd.DataFrame()
    X_full_solid = pd.DataFrame()
    for size in [11,15,21]:
        columns_blur = ['blur'+str(n)+'_'+str(size) for n in range(25)]
        columns_clear = ['clear'+str(n)+'_'+str(size) for n in range(25)]
        columns_solid = ['solid'+str(n)+'_'+str(size) for n in range(25)]
        columns = columns_blur+columns_clear
        X,X_solid,y,im_size = read_test_data(dirname,size,columns,columns_solid)
        X_full = pd.concat([X_full,X],axis =1)
        X_full_solid = pd.concat([X_full_solid,X_solid],axis =1)
    X_full = pd.concat([X_full,X_full_solid],axis =1)
    return X_full,y,im_size

def predict(target_name):
	X_test2,y_test2,size = read_full_test_data(target_name)
	y_pred = classifier.predict_proba(X_test2)[:,1]
	y_pred = y_pred.reshape((size[0]-4,size[1]-4))
	return y_pred




