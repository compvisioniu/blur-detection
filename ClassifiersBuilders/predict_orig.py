import numpy as np
import pandas as pd
import xgboost as xgb
import os
import matlab.engine


feature_list = ['LK','GHS','LPSS','LLF1','LLF2']
label_mark = 'GT'

def read_im_data_orig(dirname,size):
    size = str(size)
    one_image_data = pd.DataFrame(data=None,columns=feature_list)
    for feature_name in feature_list:
        feature_data = pd.read_csv('./extracted/'+dirname+'/'+size+'/'+feature_name+'.csv',header=-1)
        feature_size = feature_data.values.shape[0]*feature_data.values.shape[1]
        one_image_data[feature_name] = list(map(lambda x: float(x[0]),feature_data.values.transpose().reshape(feature_size,1)))
    one_image_labels = pd.read_csv('./extracted/'+dirname+'/'+size+'/'+label_mark+'.csv',header=-1)
    im_size = list(one_image_labels.shape)
    im_size[0] = im_size[0] + int(size) - 1
    im_size[1] = im_size[1] + int(size) - 1
    one_image_labels = list(map(lambda x: int(x[0]),one_image_labels.values.reshape(feature_size,1)))
    one_image_labels = pd.DataFrame(data=one_image_labels,columns=[label_mark])
    return one_image_data,one_image_labels,im_size

def convert_data_orig(one_image_data):
	converted_data = []
	for feature_name in feature_list:
		converted_data += [list(map(float,one_image_data[feature_name].values))]
	return converted_data	

def predict(dir_name):
	eng = matlab.engine.start_matlab()
	eng.addpath(r'C:\Projects\BlurProject\BlurClassification\matlab_scripst_for_validation',nargout=0)
	
	X,y,im_size = read_im_data_orig(dir_name,11)
	data11 = convert_data_orig(X)

	X,y,im_size = read_im_data_orig(dir_name,15)
	data15 = convert_data_orig(X)

	X,y,im_size = read_im_data_orig(dir_name,21)
	data21 = convert_data_orig(X)
	
	predict = eng.test(im_size,data11,data15,data21)
	predict = np.array(predict._data).reshape(predict.size[::-1]).T
	w = predict.shape[0]
	h = predict.shape[1]
	predict = predict[12:w-12,12:h-12]
	eng.quit()
	return predict	
