function result = test(imsize,data11,data15,data21)
    patchsize = [11,15,21];
    load('nb_classifier.mat');
    nb = cell(1,3);
    nb{1} = nb11; nb{2} = nb15; nb{3} = nb21;
    s1 = 11;        % patch size for scale 1
    s2 = 15;        % patch size for scale 2
    s3 = 21;        % patch size for scale 3
    alpha = 0.5;    % weight for multiscale inference
    
    im_width = imsize{2};
    im_height = imsize{1};
    
    data = cell(1,3);
    data{1} = [cell2mat(data11{1});cell2mat(data11{2});cell2mat(data11{3});cell2mat(data11{4});cell2mat(data11{5})];
    data{2} = [cell2mat(data15{1});cell2mat(data15{2});cell2mat(data15{3});cell2mat(data15{4});cell2mat(data15{5})];
    data{3} = [cell2mat(data21{1});cell2mat(data21{2});cell2mat(data21{3});cell2mat(data21{4});cell2mat(data21{5})];
    
    features = cell(1,3);
    
    for i = 1:3
        offset = (patchsize(i) - 1)/2;
        x_start = offset+1;  x_end = im_width-offset;
        y_start = offset+1;  y_end = im_height-offset;

        post = posterior(nb{i},data{i}');
        post = mat2gray(post(:,2));
        im = zeros(im_height, im_width);
        im(y_start:y_end,x_start:x_end) = reshape(post,y_end-y_start+1,x_end-x_start+1);
        features{i} = im;
    end
    
    feature.scale1 = features{1};
    feature.scale2 = features{2};
    feature.scale3 = features{3};
    
    result = multiScaleBlurInference(feature, alpha, s1, s2, s3);
    %result = size(feature.scale1);
end